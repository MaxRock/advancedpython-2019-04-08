"""
1. Каждое из слов «разработка», «сокет», «декоратор» представить в строковом формате
и проверить тип и содержание соответствующих переменных. Затем с помощью онлайн-конвертера 
преобразовать строковые представление в формат Unicode и также проверить тип и содержимое переменных.
"""
a = ['разработка', 'сокет', 'декоратор']

b = ['\xd1\x80\xd0\xb0\xd0\xb7\xd1\x80\xd0\xb0\xd0\xb1\xd0\xbe\xd1\x82\xd0\xba\xd0\xb0',
     '\xd1\x81\xd0\xbe\xd0\xba\xd0\xb5\xd1\x82',
     '\xd0\xb4\xd0\xb5\xd0\xba\xd0\xbe\xd1\x80\xd0\xb0\xd1\x82\xd0\xbe\xd1\x80']

for _ in a:
    print(f" Слово: {_}, Тип:  {type(_)}, Длина: {len(_)}")

for _ in b:
    print(f" Слово: {_}, Тип:  {type(_)}, Длина: {len(_)}")

"""
 2. Каждое из слов «class», «function», «method» записать в байтовом типе без преобразования
 в последовательность кодов (не используя методы encode и decode) и определить тип, содержимое
 и длину соответствующих переменных.
"""

a = [b'class',b'function',b'method']
for _ in a:
    print(_, type(_), len(_))

"""
3. Определить, какие из слов «attribute», «класс», «функция», «type» невозможно записать в байтовом типе.
"""

a = ['attribute','класс','функция', 'type']
for _ in a:
    try:
        print (_.encode('ASCII'))
    except:
        print (f'Слово: "{_}" невозможно записать в байтовом типе.')

"""
4. Преобразовать слова «разработка», «администрирование», «protocol», «standard» из строкового
 представления в байтовое и выполнить обратное преобразование (используя методы encode и decode).
"""

a = ['разработка','администрирование','protocol','standard']
for _ in a:
    b = _.encode('utf-8')
    print(f'Слово: "{_}" в байтовом представлении: {b}')
    c = b.decode('utf-8')
    print(f' Байтовое слово: "{b}" в строчном представлении: {c}')

"""
5. Выполнить пинг веб-ресурсов yandex.ru, youtube.com и преобразовать результаты из байтовового в строковый тип на кириллице.
"""

import subprocess

site = [['ping','-c 1','yandex.ru'],['ping','-c 1','google.ru']]
for _ in site:
    print(f'Executing: {_}')
    for line in subprocess.Popen(_,stdout=subprocess.PIPE).stdout:
        print(line.decode('utf-8'))


"""
6. Создать текстовый файл test_file.txt, заполнить его тремя строками: «сетевое программирование»,
 «сокет», «декоратор». Проверить кодировку файла по умолчанию. Принудительно открыть файл в формате
 Unicode и вывести его содержимое.
"""

file_name = 'test_file.txt'

with open(file_name, 'w') as cf:
    cf.write('сетевое программирование\n')
    cf.write('сокет\n')
    cf.write('декоратор\n')
    print(f"Кодировка файла {file_name} по умолчанию: {cf.encoding}")

with open(file_name, 'r', encoding='utf-8', errors="ignore") as rf:
    print(rf.read())






















